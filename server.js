var express = require('express');
var bodyParser = require('body-parser');
var restClient = require('node-rest-client').Client;
var avatarUrl = "https://cdn0.iconfinder.com/data/icons/robby-bolt/24/robot_peerup_droid_bot_automate-2-128.png";

var TARGET_HOOK = ''
var SECUTIRY_TOKEN = "blyps";
var PORT = (process.env.PORT || 5000);

var app =express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
var router = express.Router();
app.use('/webhook-server', router);
app.listen(PORT);
console.log('Webhook Server started... port: ' + PORT);

app.get('/', function(request, response) {
  console.log('GET request received');
  response.send('Please use POST Request');
});

router.post('/discord-hook/', function(request, response) {
	//console.log("request token: " + params.token);
	// if(request.params.token != SECUTIRY_TOKEN) {
	// 	console.log("UNAUTHORIZED!");
	// 	response.status(401).send({ error: 'Unauthorized POST Request' });
	// 	return;
	// } else {
	//	
	//}
	response.status(200).send("message":"Received");
	console.log('Received Payload: ' + JSON.stringify(request.body));
	postDiscordHook( request );

});

/* Function parses code from Source to format understood by Discord */
function pasrseToDiscordJSON( data ) {
	var discordJSON = '';

	var message = data.body.message + "\n";
	var changes = data.body.changes;
	var changeType = data.body.kind;
	
	switch (changeType) {
                
                case "comment_create_activity":
                	var storyName = '';
                	for ( var i = 0; i < changes.length; i++) {
                		if( changes[i].name != null) {
							storyName = changes[i].name;
							break;
                		}
                	}

                	message = "::" + storyName + ":: " + message;
                    break;
                
                case "story_update_activity":
                	message += " :" + changes[0].name;
                    break;
                
                case "comment_delete_activity":
                	var storyName = '';
                	for ( var i = 0; i < changes.length; i++){
                		if( changes[i].name != null) {
							storyName = changes[i].name;
							break;
                		}	
                	}
                	message = "::" + storyName + ":: " + message;
					break;

				default:
					break;
	}
	//console.log( "Name of Feature/Comment: " + name);
	// if (name != null) {
	// 	message += name;
	// }
	discordJSON =  {"content" : message , "username" : "Pivotal Tracker", "avatar_url" : avatarUrl};

	return discordJSON;
}

/* Functions posts the pasred JSON to Discord Server */
function postDiscordHook( requestBody ) {
	var rest = new restClient();
    var body = pasrseToDiscordJSON(requestBody);
	var arguments = {data: body,headers:{"Content-Type": "application/json"}};

	rest.post(TARGET_HOOK, arguments, function(data,response) {
        //console.log('Sending to destination hook: ' + JSON.stringify(arguments));
        if (response.statusCode != 200) {
            console.log('Received response: ' + response.statusCode + ' (' + response.statusMessage + ') from destination server [' + TARGET_HOOK + ']');
        } else {
        	console.log("Post Dicord Hook Success");
        }
        response.status(200).send("message":"Received");
	});
}
